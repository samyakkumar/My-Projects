public class Vector {

    public double x;
    public double y;
    public double z;

    public Vector(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static dotProduct(Vector self, Vector other) {

    }

    public static add(Vector self, Vector other){
        return Vector(self.x + other .x, self.y + other.y, self.z + other.z)
    }

    @Override
    public boolean equals(Object other){
        if (this == other) {
            return true;
        }

        if (!(other instanceof Vector)) {
            return false;
        }

        Vector x = (Vector) other;
        return (this.x == other.x && this.y == other.y
        && this.z == other.z)
    }
}
